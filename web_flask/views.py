"""
Routes
"""

from hashlib import sha256
from flask import render_template, url_for, redirect, flash, request
from flask_login import login_user, current_user, logout_user, login_required
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, PasswordField, SubmitField, BooleanField, DateField, RadioField, \
    IntegerField, TimeField
from wtforms.validators import DataRequired, Length, Email, EqualTo
from wtforms.validators import ValidationError
from .app import app
from .query import *


class RegistrationForm(FlaskForm):
    username = StringField('Pseudo', validators=[DataRequired(), Length(min=2, max=32)])
    email = StringField("Email", validators=[DataRequired(), Email()])
    password = PasswordField('Mot de passe', validators=[DataRequired(), Length(min=6, max=32)])
    confirm_password = PasswordField('Confirmer mot de passe',
                                     validators=[DataRequired(), Length(min=6, max=32), EqualTo("password")])
    submit = SubmitField("S'inscrire")
    next = HiddenField()

    def validate_username(self, username):
        user = Utilisateur.query.get(username.data)
        if user:
            raise ValidationError("Ce pseudonyme est déjà utilisé")
        return True


class LoginForm(FlaskForm):
    username = StringField('Pseudo', validators=[DataRequired(), Length(min=2, max=32)])
    password = PasswordField('Mot de passe', validators=[DataRequired(), Length(min=6, max=32)])
    remember = BooleanField("Se souvenir de moi")
    submit = SubmitField("Se connecter")
    next = HiddenField()

    def get_authenticated_user(self):
        utilisateur = Utilisateur.query.get(self.username.data)
        if utilisateur is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        if passwd == utilisateur.mdp_utilisateur:
            return utilisateur
        else:
            return None


class CreationPlaylistForm(FlaskForm):
    nom_playlist = StringField('Nom de la playlist', validators=[DataRequired(), Length(min=2, max=32)])
    description_playlist = StringField("Description", validators=[DataRequired()])
    est_public = BooleanField('Playlist visible par tout le monde')
    submit = SubmitField("Confirmer")
    next = HiddenField()


class AjoutMusiqueAlbumForm(FlaskForm):
    nom_musique = StringField('Nom de la musique', validators=[DataRequired(), Length(min=2, max=32)])
    duree_musique = TimeField('Durée de la musique', format="%H:%M:%S", validators=[DataRequired()])
    genres_musique = StringField("Nom des genres de la musique séparé par des virgule", validators=[DataRequired()])
    id_artistes = StringField("id des artistes séparés par des virgules", validators=[DataRequired()])
    submit = SubmitField("Confirmer")
    next = HiddenField()


class EnregistrementPlaylistForm(FlaskForm):
    submit = SubmitField("Enregistrer")
    next = HiddenField()


class DesenregistrementPlaylistForm(FlaskForm):
    submit = SubmitField("Désenregistrer")
    next = HiddenField()


class SuppressionMusiquePlaylistForm(FlaskForm):
    submit = SubmitField("")
    next = HiddenField()


class SuppressionPlaylistForm(FlaskForm):
    submit = SubmitField("Supprimer cette playlist")
    next = HiddenField()


class SuppressionMusiqueAlbumForm(FlaskForm):
    submit = SubmitField("")
    next = HiddenField()


class SuppressionAlbumForm(FlaskForm):
    submit = SubmitField("Supprimer cet album")
    next = HiddenField()


def edition_playlist_form(id_playlist):
    playlist = Playlist.query.get_or_404(id_playlist)

    class EditionPlaylistForm(FlaskForm):
        nom_playlist = StringField('Nom de la playlist', validators=[DataRequired(), Length(min=2, max=32)],
                                   default=playlist.nom_playlist)
        description_playlist = StringField("Description", validators=[DataRequired()],
                                           default=playlist.description_playlist)
        est_public = BooleanField('Playlist visible par tout le monde', default=playlist.est_public)
        submit = SubmitField("Confirmer")
        next = HiddenField()

    form_editer = EditionPlaylistForm()

    return form_editer


class CreationAlbumForm(FlaskForm):
    nom_album = StringField("Nom de l'album", validators=[DataRequired(), Length(min=2, max=32)])
    date_parution_album = DateField("Date de parution de l'album", format='%Y-%m-%d', validators=[DataRequired()],
                                    default=datetime.now())
    type_album = RadioField("Type d'album", choices=[('Album', 'Album'), ('Single', 'Single'), ('EP', 'EP')],
                            validators=[DataRequired()], default='Album')
    submit = SubmitField("Confirmer")
    next = HiddenField()


def edition_album_form(id_album):
    album = Album.query.get_or_404(id_album)
    if len(album.artistes) > 0:
        id_artiste = album.artistes[0].id_artiste
    else:
        id_artiste = None

    class EditionAlbumForm(FlaskForm):
        nom_album = StringField("Nom de l'album", validators=[DataRequired(), Length(min=2, max=32)],
                                default=album.nom_album)
        date_parution_album = DateField("Date de parution de l'album", format='%Y-%m-%d', validators=[DataRequired()],
                                        default=album.date_parution_album)
        nouvel_artiste = IntegerField("Id du nouvel artiste", validators=[DataRequired()],
                                      default=id_artiste)
        type_album = RadioField("Type d'album", choices=[('Album', 'Album'), ('Single', 'Single'), ('EP', 'EP')],
                                validators=[DataRequired()], default=album.type_album)
        submit = SubmitField("Confirmer")
        next = HiddenField()

    form_editer = EditionAlbumForm()

    return form_editer


@app.route('/', methods=['GET'])
def home():
    page = request.args.get("page", 1, type=int)
    albums = Album.query.paginate(page=page, per_page=16)
    playlists = get_playlists_utilisateur(current_user.get_id())
    return render_template("home.html", title="Accueil", albums=albums, playlists=playlists)


@app.route("/inscription", methods=["GET", "POST"])
def inscription():
    form = RegistrationForm()
    if form.validate_on_submit():
        m = sha256()
        m.update(form.password.data.encode())
        u = Utilisateur(pseudo_utilisateur=form.username.data, mdp_utilisateur=m.hexdigest(),
                        email_utilisateur=form.email.data)
        db.session.add(u)
        db.session.commit()
        flash("Votre compte a été créé avec succès. Vous pouvez maintenant vous connecter", "success")
        return redirect(url_for("login"))
    return render_template("inscription.html", title="Inscription", form=form)


@app.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if not form.is_submitted():
        form.next.data = request.args.get("next")
    elif form.validate_on_submit():
        user = form.get_authenticated_user()
        if user:
            flash("Connexion réussie", "success")
            login_user(user)
            next = form.next.data or url_for("home")
            return redirect(next)
        flash("Connexion impossible. Veuillez vérifier votre pseudonyme et votre mot de passe", "danger")
    return render_template("login.html", title="Connexion", form=form)


@app.route('/logout/')
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route("/albums", methods=['GET', 'POST'])
def albums():
    page = request.args.get("page", 1, type=int)
    albums = Album.query.paginate(page=page, per_page=16)
    if request.method == "POST":
        query = ""
        if "nom_artiste" in request.form:
            if request.form["nom_artiste"] and not request.form["nom_artiste"].isspace():
                if not query:
                    query += f"Album.query.join(Album.artistes, aliased=True).filter(Artiste.nom_artiste.like(\'%{request.form['nom_artiste']}%\'))"
        if "nom_album" in request.form:
            if request.form["nom_album"] and not request.form["nom_album"].isspace():
                if not query:
                    query += f"db.session.query(Album).filter(Album.nom_album.like(\'%{request.form['nom_album']}%\'))"
                else:
                    query = query[:-1]
                    query += f", Album.nom_album.like(\'%{request.form['nom_album']}%\'))"
        if "annee_parution" in request.form:
            if request.form["annee_parution"] and not request.form["annee_parution"].isspace():
                if not query:
                    query += f"db.session.query(Album).filter(Album.date_parution_album.like(\'%{request.form['annee_parution']}%\'))"
                else:
                    query = query[:-1]
                    query += f", Album.date_parution_album.like(\'%{request.form['annee_parution']}%\'))"
        if "nom_genre" in request.form:
            if request.form["nom_genre"] and not request.form["nom_genre"].isspace():
                if not query:
                    query += f"get_albums_genre(\'{request.form['nom_genre']}\')"
                else:
                    query = query[:-1]
                    liste = [album.id_album for album in get_albums_genre(request.form['nom_genre'])]
                    query += f", Album.id_album.in_({liste}))"
        if query and not query.isspace():
            albums = eval(query).paginate(page=page, per_page=16)

    playlists = get_playlists_utilisateur(current_user.get_id())

    return render_template("albums.html", title="Bibliothèque d'albums, d'EPs et de singles", albums=albums,
                           playlists=playlists)


@app.route("/album/<id_album>/<note>/<page>/<id_page>", methods=['GET'])
@login_required
def utilisateur_note_album(id_album, note, page, id_page):
    if db.session.query(Album).join(Utilisateur_note_album).join(Utilisateur).filter(Album.id_album == id_album,
                                                                                     Utilisateur.pseudo_utilisateur == current_user.pseudo_utilisateur).count() > 0:
        modifier_set(Utilisateur_note_album, nom_clef_primaire=("pseudo_utilisateur", "id_album"),
                     valeur_clef_primaire=(current_user.pseudo_utilisateur, id_album), dico_donnees={"notation": note})
    else:
        note = Utilisateur_note_album(pseudo_utilisateur=current_user.pseudo_utilisateur, id_album=id_album,
                                      notation=note)
        db.session.add(note)
        db.session.commit()
    return redirect(url_for(page, id=id_page))


@app.route("/album/<id>", methods=['GET', 'POST'])
def album(id):

    # Initialisation des formulaires
    form_album_ajout_musique = AjoutMusiqueAlbumForm()
    form_suppression_musique = SuppressionMusiqueAlbumForm()
    form_album_editer = edition_album_form(id)
    form_album_supprimer = SuppressionAlbumForm()

    # Récupération des informations de la BD
    artiste = db.session.query(Artiste).join(artiste_album).join(Album).filter(Album.id_album == id).first()
    playlists = get_playlists_utilisateur(current_user.get_id())
    playlists_perso = db.session.query(Playlist).filter(Playlist.pseudo_createur_playlist == current_user.get_id())
    album = Album.query.get_or_404(id)

    # Gestion de l'affichage d'une musique pour aligner les colonnes
    max_len = 100
    if album.musiques:
        max_len = max(len(musique.nom_musique) for musique in album.musiques)

    # Gestion de la notation d'album
    note_utilisateur = 0
    if current_user.is_authenticated:
        note_utilisateur = Utilisateur_note_album.query.get((current_user.pseudo_utilisateur, id)) or 0
        if note_utilisateur:
            note_utilisateur = note_utilisateur.notation

    # Gestion de l'ajout d'une musique à une playlist
    ajouter_musique_playlist(request.form.getlist('check'), request.form.get('musique_id'), id, "album")

    return render_template("album.html", title=album.type_album, album=album,
                           artiste_album=artiste, playlists=playlists, playlists_perso=playlists_perso,
                           id_album=id,
                           note_utilisateur=note_utilisateur, form_album_editer=form_album_editer,
                           form_suppression_musique=form_suppression_musique, max_len=max_len,
                           form_album_ajout_musique=form_album_ajout_musique,
                           form_album_supprimer=form_album_supprimer)


def ajouter_musique_playlist(liste_playlist_ids, musique_id, id, page_redirect):
    """
    Permet d'ajouter une musique à une liste de playlist
    Renvoie ensuite vers la route où a été appelé la fonctionnalité
    """
    for playlist_id in liste_playlist_ids:
        if ajout_musique_playlist(playlist_id, musique_id) is not None:
            flash("Musique déjà présente dans cette playlist", "danger")
        else:
            flash("Musique ajoutée avec succès à la playlist", "success")
    return redirect(url_for(page_redirect, id=id))


@app.route("/album/<id>/ajouter_musique", methods=['GET', 'POST'])
@login_required
def ajouter_musique_album(id):
    album = Album.query.get_or_404(id)
    if album:
        form_album_ajout_musique = AjoutMusiqueAlbumForm()
        if form_album_ajout_musique.validate_on_submit() and current_user.est_admin:
            genres_musique = form_album_ajout_musique.genres_musique.data.split(",")
            id_artistes = form_album_ajout_musique.id_artistes.data

            # Creation d'un nouveau genre si l'utilisateur en rentre un nouveau
            for genre in genres_musique:
                if not Genre.query.get(genre):
                    nouveau_genre = Genre(nom_genre=genre)
                    db.session.add(nouveau_genre)
                    db.session.commit()

            # Gestion du choix de l'artiste
            for id_artiste in id_artistes.split(","):
                if not Artiste.query.get(id_artiste):
                    flash(f"L'artiste d'id {id_artiste} n'existe pas", "danger")
                    return redirect(url_for("album", id=id))

            # Creation de la musique
            creer_musique_sans_id(form_album_ajout_musique.nom_musique.data,
                                  form_album_ajout_musique.duree_musique.data,
                                  [Genre.query.get_or_404(genre) for genre in
                                   form_album_ajout_musique.genres_musique.data.split(",")],
                                  [Artiste.query.get_or_404(id_artiste) for id_artiste in
                                   form_album_ajout_musique.id_artistes.data.split(",")], id)
            flash("Musique ajoutée avec succès", "success")
        return redirect(url_for("album", id=id))
    return redirect(url_for("albums"))


@app.route("/album/<id>/editer", methods=['GET', 'POST'])
@login_required
def album_editer(id):
    album = Album.query.get_or_404(id)
    if album:
        form_album_editer = edition_album_form(id)
        if form_album_editer.validate_on_submit() and current_user.est_admin:
            modifier_set(Album, nom_clef_primaire="id_album", valeur_clef_primaire=id,
                         dico_donnees={"nom_album": form_album_editer.nom_album.data,
                                       "date_parution_album": form_album_editer.date_parution_album.data,
                                       "type_album": form_album_editer.type_album.data}
                         )
            # Si l'id d'artiste rentré n'existe pas
            if not Artiste.query.get(form_album_editer.nouvel_artiste.data):
                flash(f"L'artiste d'id {form_album_editer.nouvel_artiste.data} n'existe pas", "danger")
                return redirect(url_for("album", id=id))

            # Si l'album possède des artistes
            if len(album.artistes) > 0:

                # On retire l'ancien artiste lié à l'album
                modifier_remove(Album, nom_clef_primaire="id_album", valeur_clef_primaire=id,
                                dico_donnees={"artistes": album.artistes[0]})

            # On lie le nouvel artiste à l'album
            modifier_add(Album, nom_clef_primaire="id_album", valeur_clef_primaire=id,
                         dico_donnees={"artistes": Artiste.query.get_or_404(form_album_editer.nouvel_artiste.data)})
            flash("Album édité avec succès", "success")
        return redirect(url_for("album", id=id))
    return redirect(url_for("albums"))


@app.route("/album/<id>/supprimer", methods=['GET', 'POST'])
@login_required
def album_supprimer(id):
    form_album_supprimer = SuppressionAlbumForm()
    if form_album_supprimer.validate_on_submit():
        supprimer_album(id)
        flash("L'album a été supprimé avec succès", "success")
    return redirect(url_for("albums"))


@app.route("/genre/<nom_genre>", methods=['GET'])
def genre(nom_genre):
    page = request.args.get("page", 1, type=int)
    albums = get_albums_genre(nom_genre).paginate(page=page, per_page=16)
    playlists = get_playlists_utilisateur(current_user.get_id())
    return render_template("genre.html", title="Genre", albums=albums, nom_genre=nom_genre,
                           playlists=playlists)


@app.route("/album/creer", methods=['GET', 'POST'])
@login_required
def creer_album():
    if not current_user.est_admin:
        return redirect(url_for("albums"))
    form = CreationAlbumForm()
    playlists = get_playlists_utilisateur(current_user.get_id())
    if form.validate_on_submit() and current_user.est_admin:
        creer_nouvel_album(nom_album=form.nom_album.data, date_parution_album=form.date_parution_album.data,
                           type_album=form.type_album.data)
        flash("Nouvel album créé avec succés", "success")
        return redirect(url_for("albums"))
    return render_template("creer_album.html", title="Rentrez les informations du nouvel album", form=form,
                           playlists=playlists)


@app.route("/artiste/<id>", methods=['GET', 'POST'])
def artiste(id):
    if db.session.query(Artiste).get_or_404(id):
        # Récupération des informations de la BD
        albums = db.session.query(Album).join(artiste_album).join(Artiste).filter(Artiste.id_artiste == id,
                                                                                  Album.type_album == "Album")
        eps = db.session.query(Album).join(artiste_album).join(Artiste).filter(Artiste.id_artiste == id,
                                                                               Album.type_album == "EP")
        singles = db.session.query(Album).join(artiste_album).join(Artiste).filter(Artiste.id_artiste == id,
                                                                                   Album.type_album == "Single")
        playlists = get_playlists_utilisateur(current_user.get_id())
        playlists_perso = db.session.query(Playlist).filter(Playlist.pseudo_createur_playlist == current_user.get_id())

        # Initialisation des dicos de notation par type d'album
        notes_utilisateur_album = {}
        notes_utilisateur_ep = {}
        notes_utilisateur_single = {}

        # Gestion de l'affichage des notes
        if current_user.is_authenticated:
            for album in albums:
                note_utilisateur = Utilisateur_note_album.query.get(
                    (current_user.pseudo_utilisateur, album.id_album)) or 0
                if note_utilisateur:
                    note_utilisateur = note_utilisateur.notation
                notes_utilisateur_album[album.id_album] = note_utilisateur
            for ep in eps:
                note_utilisateur = Utilisateur_note_album.query.get((current_user.pseudo_utilisateur, ep.id_album)) or 0
                if note_utilisateur:
                    note_utilisateur = note_utilisateur.notation
                notes_utilisateur_ep[ep.id_album] = note_utilisateur
            for single in singles:
                note_utilisateur = Utilisateur_note_album.query.get(
                    (current_user.pseudo_utilisateur, single.id_album)) or 0
                if note_utilisateur:
                    note_utilisateur = note_utilisateur.notation
                notes_utilisateur_single[single.id_album] = note_utilisateur

        # Gestion de l'ajout d'une musique à une playlist
        ajouter_musique_playlist(request.form.getlist('check'), request.form.get('musique_id'), id, "artiste")

        return render_template("artiste.html", title="Artiste", albums=albums, eps=eps,
                               singles=singles, playlists=playlists,
                               artiste_album=db.session.query(Artiste).get_or_404(id), playlists_perso=playlists_perso,
                               id_artiste=id,
                               notes_utilisateur_album=notes_utilisateur_album,
                               notes_utilisateur_ep=notes_utilisateur_ep,
                               notes_utilisateur_single=notes_utilisateur_single,
                               form_suppression_musique=SuppressionMusiqueAlbumForm())


@app.route("/artistes", methods=['GET'])
def artistes():
    page = request.args.get("page", 1, type=int)
    artistes = Artiste.query.paginate(page=page, per_page=16)
    playlists = get_playlists_utilisateur(current_user.get_id())
    return render_template("artistes.html", title="Bibliothèque d'artistes", artistes=artistes, playlists=playlists)


@app.route("/genres", methods=['GET'])
def genres():
    page = request.args.get("page", 1, type=int)
    genres = Genre.query.paginate(page=page, per_page=16)
    playlists = get_playlists_utilisateur(current_user.get_id())
    return render_template("genres.html", title="Bibliothèque de genres", genres=genres, playlists=playlists)


@app.route("/playlist/<id>", methods=['GET'])
def playlist(id):
    # Initialisation des formulaires
    form_enregistrer = EnregistrementPlaylistForm()
    form_desenregistrer = DesenregistrementPlaylistForm()
    form_suppression_musique = SuppressionMusiquePlaylistForm()
    form_supprimer = SuppressionPlaylistForm()
    form_editer = edition_playlist_form(id)

    # Récupération des informations de la BD
    playlist = Playlist.query.get_or_404(id)
    playlists = get_playlists_utilisateur(current_user.get_id())
    playlists_perso = db.session.query(Playlist).filter(Playlist.pseudo_createur_playlist == current_user.get_id())
    musiques = get_musiques_playlist(id)

    # Gestion de la tentative d'accès à une playlist privé par un autre utilisateur que le createur de celle-ci
    if not playlist.est_public and current_user.get_id() != playlist.pseudo_createur_playlist:
        flash("Tentative d'intrusion reportée", "danger")
        return redirect(url_for("playlists"))

    return render_template("playlist.html", id_playlist=id, form_editer=form_editer, form_supprimer=form_supprimer,
                           form_suppression_musique=form_suppression_musique, form_enregistrer=form_enregistrer,
                           form_desenregistrer=form_desenregistrer,
                           playlists=playlists, playlist=Playlist.query.get_or_404(id), musiques=musiques,
                           playlists_perso=playlists_perso)


@app.route("/playlist/<id>/enregistrer", methods=['GET', 'POST'])
def playlist_enregistrer(id):
    form_enregistrer = EnregistrementPlaylistForm()
    if form_enregistrer.validate_on_submit():
        enregistrer_playlist(id_playlist=id, pseudo_utilisateur=current_user.get_id())
        flash("Playlist enregistrée avec succès", "success")
    return redirect(url_for("playlist", id=id))


@app.route("/playlist/<id>/desenregistrer", methods=['GET', 'POST'])
def playlist_desenregistrer(id):
    form_desenregistrer = DesenregistrementPlaylistForm()
    if form_desenregistrer.validate_on_submit():
        desenregistrer_playlist(id_playlist=id, pseudo_utilisateur=current_user.get_id())
        flash("Playlist desenregistrée avec succès", "success")
    return redirect(url_for("playlist", id=id))


@app.route("/album/<id>/supprimer_musique", methods=['GET', 'POST'])
@login_required
def album_musique_suppression(id):
    form_suppression_musique = SuppressionMusiqueAlbumForm()
    liste_playlist_ids = request.form.getlist('check')
    musique_id = request.form.get('musique_id')
    for playlist_id in liste_playlist_ids:
        if ajout_musique_playlist(playlist_id, musique_id) is not None:
            flash("Musique déjà présente dans cette playlist", "danger")
    if form_suppression_musique.validate_on_submit() and current_user.est_admin:
        for playlist in Playlist.query.all():
            retrait_musique_playlist(playlist.id_playlist, musique_id)
        retrait_musique_album(musique_id)
        flash("Musique supprimée avec succès de l'album", "success")
    return redirect(url_for("album", id=id))


@app.route("/playlist/<id>/supprimer_musique", methods=['GET', 'POST'])
def playlist_musique_suppression(id):
    form_suppression_musique = SuppressionMusiquePlaylistForm()
    musique_id = request.form.get('musique_id')
    if form_suppression_musique.validate_on_submit():
        retrait_musique_playlist(id, musique_id)
        flash("Musique retirée avec succès", "success")
    return redirect(url_for("playlist", id=id))


@app.route("/playlist/<id>/supprimer", methods=['GET', 'POST'])
def playlist_supprimer(id):
    form_supprimer = SuppressionPlaylistForm()
    if form_supprimer.validate_on_submit():
        supprimer_playlist(id)
        flash("Playlist supprimée avec succès", "success")
    return redirect(url_for("playlists"))


@app.route("/playlist/<id>/editer", methods=['GET', 'POST'])
def playlist_editer(id):
    form_editer = edition_playlist_form(id)
    if form_editer.validate_on_submit():
        if not form_editer.est_public.data and Playlist.query.get_or_404(id).est_public:  # si la playlist devient privé
            for (_, pseudo_utilisateur) in db.session.query(utilisateur_playlist).filter(
                    utilisateur_playlist.c.id_playlist == id).all():
                desenregistrer_playlist(id_playlist=id,
                                        pseudo_utilisateur=pseudo_utilisateur)  # on désenregistre tous les utilisateurs à celle-ci

        modifier_set(Playlist, nom_clef_primaire="id_playlist", valeur_clef_primaire=id,
                     dico_donnees={"nom_playlist": form_editer.nom_playlist.data,
                                   "description_playlist": form_editer.description_playlist.data,
                                   "est_public": form_editer.est_public.data})
        flash("Playlist éditée avec succès", "success")
    return redirect(url_for("playlist", id=id))


@app.route("/playlist/creer", methods=["GET", "POST"])
@login_required
def creer_playlist():
    form = CreationPlaylistForm()
    playlists = get_playlists_utilisateur(current_user.get_id())
    if form.validate_on_submit():
        creer_nouvelle_playlist(nom_playlist=form.nom_playlist.data, est_public=form.est_public.data,
                                pseudo_createur_playlist=current_user.get_id(),
                                description_playlist=form.description_playlist.data)
        flash("Nouvelle playlist créée avec succés", "success")
        return redirect(url_for("playlists"))
    return render_template("creer_playlist.html", title="Rentrez les informations de la nouvelle playlist", form=form,
                           playlists=playlists)


@app.route("/playlists", methods=['GET'])
def playlists():
    playlists = get_playlists_utilisateur(current_user.get_id())
    playlists_visible = get_playlists_publiques(current_user.get_id())
    return render_template("playlists.html", title="Playlists publiques", playlists=playlists,
                           playlists_visible=playlists_visible)
