"""
Pour que le dossier soit considéré comme un package
"""

from .views import *
from .app import app, db
from .commands import *
from .models import *

