"""-------------------------------REQUETES-------------------------------"""
from datetime import datetime
from sqlalchemy import func
from sqlalchemy.orm import aliased
from .models import *


def creer_nouvelle_playlist(nom_playlist, est_public, pseudo_createur_playlist, description_playlist):
    playlist = Playlist(nom_playlist=nom_playlist, est_public=est_public,
                        pseudo_createur_playlist=pseudo_createur_playlist, description_playlist=description_playlist)
    db.session.add(playlist)
    db.session.commit()
    return playlist


def creer_nouvel_album(nom_album, date_parution_album, type_album):
    album = Album(nom_album=nom_album, date_parution_album=date_parution_album,
                  type_album=type_album, image_album="None.jpg")
    db.session.add(album)
    db.session.commit()
    return album


def supprimer_playlist(id_playlist):
    playlist = Playlist.query.get(id_playlist)
    db.session.delete(playlist)
    db.session.commit()


def supprimer_album(id_album):
    album = Album.query.get(id_album)
    db.session.delete(album)
    db.session.commit()


def enregistrer_playlist(id_playlist, pseudo_utilisateur):
    modifier_add(Playlist, nom_clef_primaire="id_playlist", valeur_clef_primaire=id_playlist,
                 dico_donnees={"utilisateurs_enregistres": Utilisateur.query.get(pseudo_utilisateur)})


def desenregistrer_playlist(id_playlist, pseudo_utilisateur):
    modifier_remove(Playlist, nom_clef_primaire="id_playlist", valeur_clef_primaire=id_playlist,
                    dico_donnees={"utilisateurs_enregistres": Utilisateur.query.get(pseudo_utilisateur)})


def ajout_musique_playlist(id_playlist, id_musique):
    musique_playlist = Musique_playlist(id_playlist=id_playlist, id_musique=id_musique, date_ajout=datetime.now())
    musique_existe = Musique_playlist.query.get((id_playlist, id_musique))
    if musique_existe is None:
        db.session.add(musique_playlist)
        db.session.commit()
    return musique_existe


def retrait_musique_playlist(id_playlist, id_musique):
    musique_playlist = Musique_playlist.query.filter(Musique_playlist.id_playlist == id_playlist,
                                                     Musique_playlist.id_musique == id_musique).one_or_none()
    if musique_playlist is not None:
        db.session.delete(musique_playlist)
        db.session.commit()


def retrait_musique_album(id_musique):
    musique_album = Musique.query.get(id_musique)
    musique_album.id_album = 0
    db.session.commit()


def creer_musique(id_musique, nom_musique, duree_musique, genres, artistes, album):
    nouvelle_musique = Musique(id_musique=id_musique, nom_musique=nom_musique,
                               duree_musique=duree_musique, genres=genres, artistes=artistes, id_album=album)
    db.session.add(nouvelle_musique)
    db.session.commit()
    return nouvelle_musique


def creer_musique_sans_id(nom_musique, duree_musique, genres, artistes, album):
    nouvelle_musique = Musique(nom_musique=nom_musique,
                               duree_musique=duree_musique, genres=genres, artistes=artistes, id_album=album)
    db.session.add(nouvelle_musique)
    db.session.commit()
    return nouvelle_musique


def creer_genre(nom_genre, musiques):
    nouveau_genre = Genre(nom_genre=nom_genre, musiques=musiques)
    db.session.add(nouveau_genre)
    db.session.commit()
    return nouveau_genre


def creer_album(id_album, nom_album, date_parution_album, type_album, image_album, musiques):
    nouvel_album = Album(id_album=id_album, nom_album=nom_album, date_parution_album=date_parution_album,
                         type_album=type_album,
                         image_album=image_album,
                         musiques=musiques)
    db.session.add(nouvel_album)
    db.session.commit()
    return nouvel_album


def creer_artiste(id_artiste, nom_artiste, description_artiste, image_artiste, musiques, albums):
    nouvel_artiste = Artiste(id_artiste=id_artiste, nom_artiste=nom_artiste, description_artiste=description_artiste,
                             image_artiste=image_artiste, musiques=musiques, albums=albums)
    db.session.add(nouvel_artiste)
    db.session.commit()
    return nouvel_artiste


def modifier_set(table, **args):
    if isinstance(args["nom_clef_primaire"], tuple):
        ligne = table.query.filter(getattr(table, args["nom_clef_primaire"][0]) == args["valeur_clef_primaire"][0],
                                   getattr(table, args["nom_clef_primaire"][1]) == args["valeur_clef_primaire"][
                                       1]).first()
    else:
        ligne = table.query.filter(getattr(table, args["nom_clef_primaire"]) == args["valeur_clef_primaire"]).first()

    for attribut, valeur in args["dico_donnees"].items():
        setattr(ligne, attribut, valeur)

    db.session.add(ligne)
    db.session.commit()


def modifier_add(table, **args):
    ligne = table.query.get(args["valeur_clef_primaire"])

    for attribut, valeur in args["dico_donnees"].items():
        getattr(ligne, attribut).append(valeur)
    db.session.commit()


def modifier_remove(table, **args):
    ligne = table.query.get(args["valeur_clef_primaire"])

    for attribut, valeur in args["dico_donnees"].items():
        getattr(ligne, attribut).remove(valeur)
    db.session.commit()


def recherche():
    pass


def get_artistes_album(id_album):
    """Retourne la liste des artistes de l'album passé en paramètre"""
    return Artiste.query.join(Artiste.albums, aliased=True).filter(Album.id_album == id_album).all()


def get_album_musique(id_musique):
    """Retourne l'album de la musique passée en paramètre"""
    # return Album.query.join(Album.musiques, aliased=True).filter(Musique.id_musique==id_musique).first()
    return Album.query.get(Musique.query.get(id_musique).first().id_album)


def get_musiques_album(id_album):
    """Retourne la liste des musiques de l'album passé en paramètre"""
    return Musique.query.filter(Musique.id_album == id_album).all()


def get_albums_artiste(id_artiste):
    """Retourne la liste des albums de l'artiste passé en paramètre"""
    return Album.query.join(Album.artistes, aliased=True).filter(Artiste.id_artiste == id_artiste).all()


def get_albums_nom_artiste(nom_artiste):
    """Retourne la liste des albums de l'artiste passé en paramètre"""
    return Album.query.join(Album.artistes, aliased=True).filter(Artiste.nom_artiste == nom_artiste)


def get_artistes_musique(id_musique):
    """Retourne la liste des artistes de la musique passée en paramètre"""
    return Artiste.query.join(Artiste.musiques, aliased=True).filter(Musique.id_musique == id_musique).all()


def get_musiques_genre(nom_genre):
    """Retourne la liste des musiques du genre passé en paramètre"""
    return Musique.query.join(Musique.genres, aliased=True).filter(Genre.nom_genre == nom_genre).all()


def get_genres_musique(id_musique):
    """Retourne la liste des genres de la musique passée en paramètre"""
    return Genre.query.join(Genre.musiques, aliased=True).filter(Musique.id_musique == id_musique).all()


def get_albums_genre(nom_genre):
    a1 = aliased(Album)
    a2 = aliased(Album)
    return db.session.query(a1) \
        .join(Musique) \
        .join(genre_musique) \
        .join(Genre) \
        .filter(Genre.nom_genre == nom_genre) \
        .group_by(a1.nom_album, Genre.nom_genre) \
        .having(func.count(Genre.nom_genre) ==
                db.session.query(func.count(Genre.nom_genre)) \
                .join(genre_musique) \
                .join(Musique) \
                .join(a2) \
                .filter(a1.id_album == a2.id_album) \
                .group_by(a2.nom_album, Genre.nom_genre) \
                .order_by(func.count(Genre.nom_genre).desc())
                .limit(1))


def get_genres_album(id_album):
    a1 = aliased(Album)
    a2 = aliased(Album)
    return db.session.query(Genre) \
        .join(genre_musique) \
        .join(Musique) \
        .join(a1) \
        .filter(a1.id_album == id_album) \
        .group_by(a1.nom_album, Genre.nom_genre) \
        .having(func.count(Genre.nom_genre) ==
                db.session.query(func.count(Genre.nom_genre)) \
                .join(genre_musique) \
                .join(Musique) \
                .join(a2) \
                .filter(a1.id_album == a2.id_album) \
                .group_by(a2.nom_album, Genre.nom_genre) \
                .order_by(func.count(Genre.nom_genre).desc())
                .limit(1))


def get_genres_nom_album(nom_album):
    a1 = aliased(Album)
    a2 = aliased(Album)
    return db.session.query(Genre) \
        .join(genre_musique) \
        .join(Musique) \
        .join(a1) \
        .filter(a1.nom_album == nom_album) \
        .group_by(a1.nom_album, Genre.nom_genre) \
        .having(func.count(Genre.nom_genre) ==
                db.session.query(func.count(Genre.nom_genre)) \
                .join(genre_musique) \
                .join(Musique) \
                .join(a2) \
                .filter(a1.id_album == a2.id_album) \
                .group_by(a2.nom_album, Genre.nom_genre) \
                .order_by(func.count(Genre.nom_genre).desc())
                .limit(1))


def get_musiques_playlist(id_playlist):
    """Retourne la liste des musiques de la playlist passée en paramètre"""
    return Musique.query.join(Musique.playlists).filter(Musique_playlist.id_playlist == id_playlist).all()


def get_playlists_utilisateur(pseudo_utilisateur):
    """Retourne la liste des playlists créées par l'utilisateur passé en paramètre ainsi que les playlists publiques
    enregistrées """
    playlists_perso = db.session.query(Playlist).filter(Playlist.pseudo_createur_playlist == pseudo_utilisateur).all()
    playlists_saved = db.session.query(Playlist).join(utilisateur_playlist).filter(
        utilisateur_playlist.c.pseudo_utilisateur == pseudo_utilisateur).all()
    playlists_perso.extend(playlists_saved)
    return playlists_perso


def get_playlists_publiques(pseudo_utilisateur):
    """Retourne la liste des playlists publiques en excluant les playlists créés par l'user"""
    playlists_perso = db.session.query(Playlist).filter(Playlist.pseudo_createur_playlist == pseudo_utilisateur)
    playlists_public = db.session.query(Playlist).filter(Playlist.est_public)
    playlists_visible = list(set(playlists_public) - set(playlists_perso))
    return playlists_visible
