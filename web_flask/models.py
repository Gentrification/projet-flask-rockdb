from .app import db, login_manager
from flask_login import UserMixin

genre_musique = db.Table('genre_musique',
                         db.Column('id_musique', db.Integer, db.ForeignKey('musique.id_musique'), primary_key=True),
                         db.Column('nom_genre', db.Integer, db.ForeignKey('genre.nom_genre'), primary_key=True)
                         )

artiste_musique = db.Table('artiste_musique',
                           db.Column('id_musique', db.Integer, db.ForeignKey('musique.id_musique'), primary_key=True),
                           db.Column('id_artiste', db.Integer, db.ForeignKey('artiste.id_artiste'), primary_key=True)
                           )

artiste_album = db.Table('artiste_album',
                         db.Column('id_album', db.Integer, db.ForeignKey('album.id_album'), primary_key=True),
                         db.Column('id_artiste', db.Integer, db.ForeignKey('artiste.id_artiste'), primary_key=True)
                         )

utilisateur_playlist = db.Table('utilisateur_playlist',
                                db.Column('id_playlist', db.Integer, db.ForeignKey('playlist.id_playlist'),
                                          primary_key=True),
                                db.Column('pseudo_utilisateur', db.Integer,
                                          db.ForeignKey('utilisateur.pseudo_utilisateur'), primary_key=True)
                                )


class Utilisateur(db.Model, UserMixin):
    pseudo_utilisateur = db.Column(db.String(32), primary_key=True)
    mdp_utilisateur = db.Column(db.String(32))
    email_utilisateur = db.Column(db.String)
    est_admin = db.Column(db.Boolean)
    playlists = db.relationship("Playlist", secondary=utilisateur_playlist)
    albums = db.relationship("Utilisateur_note_album",
                             back_populates="utilisateur", cascade="save-update, merge, delete, "
                                                                   "delete-orphan")

    def get_id(self):
        return self.pseudo_utilisateur

    def __repr__(self):
        return f"pseudo : {self.pseudo_utilisateur}, mdp : {self.mdp_utilisateur}, email : {self.email_utilisateur}" \
               f", est admin : {self.est_admin}"


class Playlist(db.Model):
    id_playlist = db.Column(db.Integer, primary_key=True)
    nom_playlist = db.Column(db.String(64))
    est_public = db.Column(db.Boolean)
    pseudo_createur_playlist = db.Column(db.String(32), db.ForeignKey("utilisateur.pseudo_utilisateur"))
    description_playlist = db.Column(db.String(128))
    utilisateurs_enregistres = db.relationship("Utilisateur", secondary=utilisateur_playlist)
    musiques = db.relationship("Musique_playlist", back_populates="playlist", cascade="save-update, merge, delete, "
                                                                                      "delete-orphan")

    def __repr__(self):
        return f"Nom playlist : {self.nom_playlist}, est public : {self.est_public}," \
               f" pseudo créateur : {self.pseudo_createur_playlist}" \
               f", description playlist : {self.description_playlist}"


class Genre(db.Model):
    nom_genre = db.Column(db.String(32), primary_key=True)
    musiques = db.relationship("Musique", secondary=genre_musique)

    def __repr__(self):
        return f"nom_genre : {self.nom_genre}"


class Musique(db.Model):
    id_musique = db.Column(db.Integer, primary_key=True)
    nom_musique = db.Column(db.String(64))
    duree_musique = db.Column(db.Time)
    playlists = db.relationship("Musique_playlist", back_populates="musique", cascade="save-update, merge, delete, "
                                                                                      "delete-orphan")
    genres = db.relationship("Genre", secondary=genre_musique)
    artistes = db.relationship("Artiste", secondary=artiste_musique)
    id_album = db.Column(db.Integer, db.ForeignKey("album.id_album"))

    def __repr__(self):
        return f"Musique : {self.nom_musique}, duree : {self.duree_musique}, " \
               f"genres : {[elem.nom_genre for elem in self.genres]}, " \
               f"artistes : {[elem.nom_artiste for elem in self.artistes]}"


class Musique_playlist(db.Model):
    id_playlist = db.Column(db.Integer, db.ForeignKey("playlist.id_playlist"), primary_key=True)
    id_musique = db.Column(db.Integer, db.ForeignKey("musique.id_musique"), primary_key=True)
    date_ajout = db.Column(db.DateTime)
    playlist = db.relationship("Playlist", back_populates="musiques")
    musique = db.relationship("Musique", back_populates="playlists")

    def __repr__(self):
        return f"id_playlist : {self.id_playlist}, id_musique : {self.id_musique}, date_ajout : {self.date_ajout}"


class Album(db.Model):
    id_album = db.Column(db.Integer, primary_key=True)
    nom_album = db.Column(db.String(64))
    date_parution_album = db.Column(db.Date)
    type_album = db.Column(db.String(16))
    image_album = db.Column(db.String(128))
    musiques = db.relationship("Musique", backref="album", lazy=True, cascade="save-update, merge, delete, "
                                                                              "delete-orphan")
    artistes = db.relationship("Artiste", secondary=artiste_album)
    utilisateurs = db.relationship("Utilisateur_note_album", back_populates="album",
                                   cascade="save-update, merge, delete, "
                                           "delete-orphan")

    def __repr__(self):
        return f"Album : {self.nom_album}, " \
               f"date_parution_album : {self.date_parution_album}, " \
               f"id_album : {self.id_album}, " \
               f"de type : {self.type_album}, " \
               f"URL image : {self.image_album}, " \
               f"musiques : {self.musiques}"


class Artiste(db.Model):
    id_artiste = db.Column(db.Integer, primary_key=True)
    nom_artiste = db.Column(db.String(32))
    description_artiste = db.Column(db.String(512))
    image_artiste = db.Column(db.String(128))
    albums = db.relationship("Album", secondary=artiste_album)
    musiques = db.relationship("Musique", secondary=artiste_musique)

    def __repr__(self):
        return f"Nom artiste : {self.nom_artiste}"


class Utilisateur_note_album(db.Model):
    pseudo_utilisateur = db.Column(db.String(32), db.ForeignKey("utilisateur.pseudo_utilisateur"), primary_key=True)
    id_album = db.Column(db.Integer, db.ForeignKey("album.id_album"), primary_key=True)
    notation = db.Column(db.Integer)
    utilisateur = db.relationship("Utilisateur", back_populates="albums")
    album = db.relationship("Album", back_populates="utilisateurs")

    def __repr__(self):
        return f"pseudo_utilisateur : {self.pseudo_utilisateur}, id_album : {self.id_album}, notation : {self.notation}"


@login_manager.user_loader
def load_user(username):
    return Utilisateur.query.get(username)
