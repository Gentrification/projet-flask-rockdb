import subprocess
import unittest
from .query import *


class TestBD(unittest.TestCase):
    def test_select(self):
        # on réinitialise la BD
        subprocess.run(["flask", "syncdb"])
        # on insert les albums dans la BD
        subprocess.run(["flask", "loaddb", "web_flask/static/album.yml"])
        # on vérifie que les albums sont dans la BD
        self.assertEqual("Musique : My Prayer, duree : 00:02:29, genres : ['Rock', 'Punk'], artistes : ['Superdrag']",
                         str(Musique.query.get(1)))
        self.assertEqual("Album : Forever Valentine, date_parution_album : 2012-08-09, id_album : 10770, de type : "
                         "Album, URL image : None, musiques : [Musique : Don't Wanna Know Why, duree : 00:04:03, "
                         "genres : ['Alternative country'], artistes : ["
                         "'Whiskeytown'], Musique : Easy Hearts, duree : 00:04:01, genres : ['Alternative country'], "
                         "artistes : ['Whiskeytown']]",
                         str(Album.query.get(10770)))
        self.assertEqual("[Nom artiste : Superdrag, Nom artiste : 16 Horsepower, Nom artiste : Ryan Adams, "
                         "Nom artiste : Whiskeytown, Nom artiste : Jesse Malin, Nom artiste : The Finger, Nom artiste "
                         ": Joan Baez, Nom artiste : Willie Nelson, Nom artiste : Cowboy Junkies, Nom artiste : "
                         "Alabama Shakes, Nom artiste : Terry Allen, Nom artiste : X, Nom artiste : Dave Alvin]",
                         str(Artiste.query.all()))
        self.assertEqual("[nom_genre : Rock, nom_genre : Punk, nom_genre : Alternative country, nom_genre : Neofolk, "
                         "nom_genre : Country, nom_genre : Folk, nom_genre : Country rock, nom_genre : Heavy metal, "
                         "nom_genre : Hard rock, nom_genre : Blues rock, nom_genre : Roots rock, nom_genre : Soul, "
                         "nom_genre : Southern rock, nom_genre : Garage rock, nom_genre : Americana, nom_genre : "
                         "Alternative rock, nom_genre : Folk rock]",
                         str(Genre.query.all()))
        self.assertEqual("nom_genre : Country",
                         str(Genre.query.get("Country")))
        # on s'assure qu'il n'y a pas de playlist lors de l'initialisation de la BD
        self.assertEqual(None,
                         Playlist.query.get(1))
        # on s'assure qu'il n'y a pas d'utilisateur lors de l'initialisation de la BD
        self.assertEqual("[]",
                         str(Utilisateur.query.all()))
        self.assertEqual("[Musique : Signal Fade, duree : 00:02:49, genres : ['Heavy metal', 'Hard rock'], artistes : "
                         "['Ryan Adams']]",
                         str(get_musiques_genre("Heavy metal")))
        self.assertEqual("[nom_genre : Alternative country, nom_genre : Neofolk]",
                         str(get_genres_musique(2)))

    def test_create(self):
        self.assertEqual(True, True)

    def test_update(self):
        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()
