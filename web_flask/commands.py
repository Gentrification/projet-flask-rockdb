from datetime import time
from hashlib import sha256
import click
import yaml
from .app import app
from .query import *


@app.cli.command()
def syncdb():
    """ Creates the tables"""
    db.drop_all()
    db.create_all()


@app.cli.command()
@click.argument('filename')
def loaddb(filename):
    """ Populates the tables with data """
    # Creation d'un admin qui peut editer les albums
    m = sha256()
    pw_admin = "adminadmin"
    m.update(pw_admin.encode())
    u = Utilisateur(pseudo_utilisateur="admin", mdp_utilisateur=m.hexdigest(), email_utilisateur="admin@gmail.com",
                    est_admin=True)
    db.session.add(u)
    db.session.commit()
    # Lecture du fichier yml
    url_album = filename
    albums = yaml.load(open(url_album), Loader=yaml.SafeLoader)
    # Ecriture dans la BD
    for album in albums:
        # on verifie l'existence de l'album dans la BD
        if not Album.query.get(album["id_album"]):
            album_actuel = creer_album(id_album=album["id_album"],
                                       nom_album=album["nom_album"],
                                       date_parution_album=album["date_parution_album"],
                                       type_album=album["type_album"],
                                       image_album=album["image_album"],
                                       musiques=[])
            for musique in album["musiques"]:
                # on verifie l'existence de la musique dans la BD
                if not Musique.query.get(musique["id_musique"]):
                    durees = [int(elem) for elem in musique["duree_musique"].split(" ")]
                    musique_actuelle = creer_musique(id_musique=musique["id_musique"],
                                                     nom_musique=musique["nom_musique"],
                                                     duree_musique=time(durees[0], durees[1], durees[2]),
                                                     genres=[],
                                                     artistes=[],
                                                     album=album_actuel.id_album)
                    for genre in musique["genre_musique"]:
                        # on verifie l'existence du genre dans la BD
                        if not Genre.query.get(genre["nom_genre"].lower()) and not Genre.query.get(genre):
                            creer_genre(nom_genre=genre["nom_genre"], musiques=[musique_actuelle])
                        else:
                            """Appel d'une fonction qui modifie la table Genre ce qui permet d'ajouter une musique à un 
                            genre déjà existant """
                            modifier_add(Genre, nom_clef_primaire="nom_genre", valeur_clef_primaire=genre["nom_genre"],
                                         dico_donnees={"musiques": musique_actuelle})
                    for artiste in musique["artistes"]:
                        if not Artiste.query.get(artiste["id_artiste"]):
                            creer_artiste(id_artiste=artiste["id_artiste"],
                                          nom_artiste=artiste["nom_artiste"],
                                          description_artiste=artiste["description_artiste"],
                                          image_artiste=artiste["image_artiste"],
                                          musiques=[musique_actuelle],
                                          albums=[album_actuel])
                        else:
                            """Appel d'une fonction qui modifie la table Artiste ce qui permet d'ajouter une musique à 
                            un artiste déjà existant """
                            modifier_add(Artiste, nom_clef_primaire="id_artiste",
                                         valeur_clef_primaire=artiste["id_artiste"],
                                         dico_donnees={"albums": album_actuel, "musiques": musique_actuelle})


@app.cli.command()
@click.argument('username')
@click.argument('password')
def nouvel_utilisateur(username, password):
    m = sha256()
    m.update(password.encode())
    u = Utilisateur(pseudo_utilisateur=username, mdp_utilisateur=m.hexdigest())
    db.session.add(u)
    db.session.commit()
