Projet Flask-RockDB par Xavier Brun & Léonard Delêtre
-----

Ce projet scolaire vise à réaliser un site web Flask qui fait office de catalogue d'albums de musiques  

Environnement d'installation
-----
  - Forker puis cloner le projet en local
  - Créer un environnement virtuel Python 3.x comme virtualenv
  - Installer les packages nécessaires
  - Initialiser la BD
  - Peupler la BD
  - **VIDER LE CACHE DU NAVIGATEUR**
  - Lancer flask

Exemple de processus
-----
  - virtualenv venv ou virtualenv -p python3 venv ou python -m venv venv
  - source venv/bin/activate ou source venv/Scripts/activate
  - cd projet-flask-rockdb (racine du projet)
  - pip install -r requirements.txt
  - flask syncdb (ignorer le warning)
  - flask loaddb web_flask/static/album.yml
  - **VIDER LE CACHE DU NAVIGATEUR**
  - flask run

Administration du site
-----
Un admin est créé automatiquement lorsque la commande "loaddb" est invoqué. Identifiant : "admin" | mdp : "adminadmin"

Seul un administrateur a accès aux fonctionnalités suivantes :
  - Création d'album (depuis la page "Bibliothèque")
  - Édition d'album (depuis la page d'un album spécifique)
  - Suppression d'album (depuis la page d'un album spécifique)
  - Ajout d'une musique à un album (depuis la page d'un album spécifique)
  - Suppression d'une musique d'un album (depuis la page d'un album spécifique ou d'un artiste)
